import logging
import time
import json
import pandas as pd
import numpy as np
import pickle
import sys
import os

from kafka import KafkaConsumer
from kafka.structs import OffsetAndMetadata, TopicPartition
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.calibration import CalibratedClassifierCV
from sklearn.metrics import top_k_accuracy_score
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import f1_score
from sklearn.metrics import classification_report
from imblearn.over_sampling import SVMSMOTE
from joblib import dump
from minio import Minio
from minio.error import InvalidResponseError
from minio.error import S3Error
from prometheus_client import pushadd_to_gateway
from prometheus_client.core import GaugeMetricFamily, REGISTRY
from datetime import datetime

logger = logging.getLogger('sensorgenova_trainer')
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

HOST_URI = os.getenv('BUCKET_ENDPOINT_HOST_URI', default='localhost:9000')
BUCKET_NAME = os.getenv('BUCKET_NAME', default='opensegnalazioni-model-bucket')
ACCESS_KEY = os.getenv('BUCKET_ENDPOINT_ACCESS_KEY_ID', default='AKIAIOSFODNN7EXAMPLE')
SECRET_KEY = os.getenv('BUCKET_ENDPOINT_SECRET_ACCESS_KEY', default='wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY')
REGION = os.getenv('BUCKET_ENDPOINT_REGION', default=None)
PUSHGATEWAY_HOSTNAME = os.getenv('PROM_PUSHGATEWAY_HOSTNAME', default='localhost')
PUSHGATEWAY_PORT = os.getenv('PROM_PUSHGATEWAY_PORT', default='9091')
PUSHGATEWAY_JOB = os.getenv('PROM_PUSHGATEWAY_JOB', default='trainer')
KAFKA_TOPIC_SEGNALAZIONI_CLEAN = os.getenv('KAFKA_TOPIC_SEGNALAZIONI_CLEAN', default='segnalazioni_clean')
KAFKA_BROKERS = os.getenv('KAFKA_BROKERS', default='localhost:29092')
KAFKA_CONSUMER_GROUP_ID = os.getenv('KAFKA_CONSUMER_GROUP_ID', default='consumer-group-trainer')
KAFKA_AUTO_OFFSET_RESET = os.getenv('KAFKA_AUTO_OFFSET_RESET', default='earliest')

class Message:
    def __init__(self, client, record):
        self._client = client
        self._record = record

    def __str__(self):
        return json.dumps(json.loads(self._record.value)).encode('utf8')

    def commit(self):
        tp = TopicPartition(topic=self._record.topic, partition=self._record.partition)
        om = OffsetAndMetadata(offset=self._record.offset + 1, metadata='')
        try:
            self._client.commit(offsets={tp:om})
        except Exception:
            logger.exception('An error occurred.')

class CustomCollector(object):
    def __init__(self):
        pass

    def collect(self):
        g_top_1_acc = GaugeMetricFamily("predictions_success_top1_total", '', labels=['modelName', 'modelVersion'])
        g_top_1_acc.add_metric(["sensor1", datetime.today().strftime('%Y%m%d')], Worker.get_metrics()['top_1_acc'])
        yield g_top_1_acc

        g_top_2_acc = GaugeMetricFamily("predictions_success_top2_total", '', labels=['modelName', 'modelVersion'])
        g_top_2_acc.add_metric(["sensor1", datetime.today().strftime('%Y%m%d')], Worker.get_metrics()['top_2_acc'])
        yield g_top_2_acc

        g_top_3_acc = GaugeMetricFamily("predictions_success_top3_total", '', labels=['modelName', 'modelVersion'])
        g_top_3_acc.add_metric(["sensor1", datetime.today().strftime('%Y%m%d')], Worker.get_metrics()['top_3_acc'])
        yield g_top_3_acc
        
        g_mrr = GaugeMetricFamily("predictions_mean_reciprocal_rank_3", '', labels=['modelName', 'modelVersion', 'rank'])
        g_mrr.add_metric(["sensor1", datetime.today().strftime('%Y%m%d'), "3"], Worker.get_metrics()['mrr_at_3'])
        yield g_mrr

        g_top_1_precision = GaugeMetricFamily("predictions_precision_top1_total", '', labels=['modelName', 'modelVersion'])
        g_top_1_precision.add_metric(["sensor1", datetime.today().strftime('%Y%m%d')], Worker.get_metrics()['top_1_precision'])
        yield g_top_1_precision

        g_top_1_recall = GaugeMetricFamily("predictions_recall_top1_total", '', labels=['modelName', 'modelVersion'])
        g_top_1_recall.add_metric(["sensor1", datetime.today().strftime('%Y%m%d')], Worker.get_metrics()['top_1_recall'])
        yield g_top_1_recall

        g_top_1_f1score = GaugeMetricFamily("predictions_f1score_top1_total", '', labels=['modelName', 'modelVersion'])
        g_top_1_f1score.add_metric(["sensor1", datetime.today().strftime('%Y%m%d')], Worker.get_metrics()['top_1_f1score'])
        yield g_top_1_f1score

        g_train_err = GaugeMetricFamily("training_error", '', labels=['modelName', 'modelVersion'])
        g_train_err.add_metric(["sensor1", datetime.today().strftime('%Y%m%d')], Worker.get_metrics()['train_err'])
        yield g_train_err

        g_test_err = GaugeMetricFamily("testing_error", '', labels=['modelName', 'modelVersion'])
        g_test_err.add_metric(["sensor1", datetime.today().strftime('%Y%m%d')], Worker.get_metrics()['test_err'])
        yield g_test_err

        c_train_data_size = GaugeMetricFamily("model_training_data_size", '', labels=['modelName', 'modelVersion'])
        c_train_data_size.add_metric(["sensor1", datetime.today().strftime('%Y%m%d')], Worker.get_metrics()['train_size'])
        yield c_train_data_size

        c_test_data_size = GaugeMetricFamily("model_testing_data_size", '', labels=['modelName', 'modelVersion'])
        c_test_data_size.add_metric(["sensor1", datetime.today().strftime('%Y%m%d')], Worker.get_metrics()['test_size'])
        yield c_test_data_size


class Consumer:
    client_class = KafkaConsumer
    message_class = Message
    consumer_timeout_ms = 3000

    def __init__(self, *, client_id):
        self._client_id = client_id

    def __enter__(self):
        self._client = self.client_class(
            KAFKA_TOPIC_SEGNALAZIONI_CLEAN,
            bootstrap_servers=[broker for broker in KAFKA_BROKERS.split(',')],
            client_id=self._client_id,
            group_id=KAFKA_CONSUMER_GROUP_ID,
            auto_offset_reset='earliest',
            enable_auto_commit=False,
            consumer_timeout_ms=self.consumer_timeout_ms,
        )
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        try:
            self._client.close()
        except Exception:
            logger.exception('An error occurred.')

    @property
    def messages(self):
        for record in self._client:
            yield self.message_class(self._client, record)


class Worker:
    consumer_class = Consumer
    clock = time
    metrics = {}
    max_retries = 10
    
    def __init__(self, *, client_id, max_messages=None):
        self._client_id = client_id
        self._max_messages = max_messages or float('inf')
        self._running = True

    def __str__(self):
        return self._client_id

    @property
    def _is_running(self):
        return self._running and self._max_messages > 0

    def start(self):
        while self._is_running:
            self._run()
                
    def _run(self):
        segnalazioni_df = pd.DataFrame(columns=['subject_description','categoryId','parentCategoryId','categoryName','status'])
        retries = 0
        with self.consumer_class(client_id=self._client_id) as consumer:
            while self._is_running:
                try:
                    message = next(consumer.messages)
                except StopIteration:
                    print('StopIteration')
                    if len(segnalazioni_df) == 0 and retries <= self.max_retries:
                        logging.info('continue')
                        retries+=1
                        continue
                    else:
                        self._running = False
                else:
                    segnalazioni_df = segnalazioni_df.append(self._process(message), ignore_index=True)
                    self._max_messages -= 1
            if(len(segnalazioni_df) == 0):
                sys.exit("Error: No message has been found in the topic")
            logging.info(f'All messages have been consumed')
            print(segnalazioni_df)
            logging.info(f'Filtering out open issues...')
            segnalazioni_df = self.filter_open_issues(segnalazioni_df)
            print(segnalazioni_df)

            logging.info(f'Dropping duplicates...')
            segnalazioni_df = self.remove_duplicates(segnalazioni_df)
            print(segnalazioni_df)
            
            logging.info(f'Getting the parent category names...')
            segnalazioni_df, parent_categories = self.get_parentcat_name(segnalazioni_df)
            print(segnalazioni_df)

            logging.info("\nSTART PARENT CATEGORY LEVEL")
            Worker.metrics['level'] = 'parent'
            model, X_train, Y_train, x_test, y_test = self.generate_model(segnalazioni_df, 'parentCategoryId', 0.94, parent_categories)
            self.get_train_test_error(model, X_train, Y_train, x_test, y_test)
            logging.info("Pushing Parent Level Metrics to Prometheus...")        
            REGISTRY.register(CustomCollector())
            pushadd_to_gateway(PUSHGATEWAY_HOSTNAME + ":" + PUSHGATEWAY_PORT, job=PUSHGATEWAY_JOB, grouping_key={'level':Worker.metrics['level']}, registry=CustomCollector())
            logging.info("END PARENT CATEGORY LEVEL")

            logging.info("\nSTART CHILD CATEGORY LEVEL")
            Worker.metrics['level'] = 'child'
            model, X_train, Y_train, x_test, y_test = self.generate_model(segnalazioni_df, 'categoryId', 0.75, parent_categories)
            self.get_train_test_error(model, X_train, Y_train, x_test, y_test)
            logging.info("Pushing Child Level Metrics to Prometheus...")        
            pushadd_to_gateway(PUSHGATEWAY_HOSTNAME + ":" + PUSHGATEWAY_PORT, job=PUSHGATEWAY_JOB, grouping_key={'level':Worker.metrics['level']}, registry=CustomCollector())
            logging.info("END CHILD CATEGORY LEVEL")
        
    @staticmethod    
    def generate_model(segnalazioni_df, cat, size_threshold, parent_categories):
        logging.info(f'Filtering out categories with low frequency...')
        segnalazioni_df = Worker.filter_data_cat(segnalazioni_df, cat, size_threshold, parent_categories)
        print(segnalazioni_df)
        
        logging.info(f'Generating category JSON files...')
        if(cat == "parentCategoryId"):
            Worker.generate_parent_category_json(segnalazioni_df)
        elif(cat == "categoryId"):
            Worker.generate_child_category_json(segnalazioni_df)
    
        logging.info(f'Starting training process...this will take a while :/')
        model, vectorizer, X_train, Y_train, x_test, y_test = Worker.train_model(segnalazioni_df, cat)
        logging.info(f'Training completed.')
        
        logging.info(f'Starting testing and evaluation process...')
        accuracy, mrr_at_k, preds = Worker.test_evaluate_model(model, x_test, y_test)
        logging.info("Top-3 Accuracy: " + str(accuracy))
        logging.info("MRR at 3: " + str(mrr_at_k))
        logging.info("Done testing and evaluation.")

        logging.info("Generating joblib files for model and vectorizer...")
        Worker.save_to_disk(model, vectorizer, cat)

        return model, X_train, Y_train, x_test, y_test

    def _pause(self):
        self.clock.sleep(1)

    def stop(self):
        self._running = False
    
    @staticmethod
    def filter_open_issues(df):
        return df[df['status'] == 'close']
    
    @staticmethod
    def remove_duplicates(df):
        df = df.drop_duplicates()
        return df

    @staticmethod
    def get_parentcat_name(df):
        df['parentCategoryName'] = df['categoryId']
        parent_categories = df[['categoryId','categoryName']][df['parentCategoryId'] == 0].drop_duplicates()
        df['parentCategoryId'][df['parentCategoryId'] == 0] = df['categoryId']
        df['parentCategoryName'] = df['parentCategoryId'].apply(lambda x: np.array(parent_categories['categoryName'][parent_categories['categoryId'] == x])[0] if x in np.array(parent_categories['categoryId']) else x)
        return df, parent_categories

    @staticmethod
    def filter_data_cat(df, cat, threshold, parent_categories):
        #Step 0: get the dataset size
        df_size = len(df)

        #Step 1: get rid of "useless" categories
        if(cat == 'categoryId'):
            category_ids_to_delete = list(parent_categories['categoryId'])
            category_ids_to_delete.append(692)
            df = df[~df[cat].isin(category_ids_to_delete)]

        #Step 2: get the frequency of each category and save it {key: cat, value: freq}
        freq = pd.DataFrame(df[cat].value_counts())
        freq = freq.rename(columns={str(cat) : 'freq'})

        #Step 3: loop over the category frequencies ordered desc and sum up to the threshold dataset size
        freq_sum = 0
        freq_last_cat = 0
        for index, row in freq.iterrows():
            dataset_size_perc = freq_sum / df_size
            if(dataset_size_perc < threshold):
                freq_sum += row['freq']
                freq_last_cat = row['freq']

        #Step 4: filter the dataset based on the categories found in step 3
        df = df[df[[cat]].replace(df[[cat]].apply(pd.Series.value_counts)).gt(freq_last_cat).all(1)]

        return df

    @staticmethod
    def generate_parent_category_json(df):
        label_encoder = preprocessing.LabelEncoder()
        df['parent_label'] = label_encoder.fit_transform(np.ravel(df['parentCategoryId'], order='C'))
        parent_cat_encoding = df[['parentCategoryId', 'parentCategoryName', 'parent_label']]
        parent_cat_encoding = parent_cat_encoding.drop_duplicates(subset=['parent_label'])
        parent_cat_encoding = parent_cat_encoding.reset_index(drop=True)
        parent_cat_encoding = parent_cat_encoding.sort_values(by=['parentCategoryId'])
        parent_cat_encoding.to_json(os.getcwd() + '/model_data/parent_cat_encoding_close.json', orient='records')
    
    @staticmethod
    def generate_child_category_json(df):
        label_encoder = preprocessing.LabelEncoder()
        df['label'] = label_encoder.fit_transform(np.ravel(df['categoryId'], order='C'))
        df['parent_label'] = label_encoder.fit_transform(np.ravel(df['parentCategoryId'], order='C'))
        cat_encoding = df[['categoryId', 'categoryName', 'label', 'parentCategoryId', 'parent_label']]
        cat_encoding = cat_encoding.drop_duplicates(subset=['label'])
        cat_encoding = cat_encoding.reset_index(drop=True)
        cat_encoding = cat_encoding.sort_values(by=['categoryId'])
        cat_encoding.to_json(os.getcwd() + '/model_data/cat_encoding_close.json', orient='records')

    @staticmethod
    def train_model(df, cat):
        X = df['subject_description']
        y = df[cat]
        
        X_train, x_test, Y_train, y_test = train_test_split(X,y,test_size=0.3,random_state=42,stratify=y)
        Worker.metrics['train_size'] = len(X_train)
        Worker.metrics['test_size'] = len(x_test)

        logger.info(f'Encoding the labels...')
        label_encoder = preprocessing.LabelEncoder() 
        Y_train = label_encoder.fit_transform(np.ravel(Y_train, order='C'))
        y_test = label_encoder.transform(np.ravel(y_test, order='C'))

        logger.info(f'Converting text in numerical representation...')
        vectorizer = TfidfVectorizer(min_df=5, max_df=0.5, max_features=None, ngram_range=(1,2), use_idf=True, norm='l2')
        X_train = vectorizer.fit_transform(X_train)
        x_test = vectorizer.transform(x_test)

        logger.info(f'Resampling the dataset...')
        resampler = SVMSMOTE()
        X_train, Y_train = resampler.fit_resample(X_train, Y_train)

        logger.info(f'Training the model...')
        model = CalibratedClassifierCV(base_estimator=LogisticRegression(solver='newton-cg', class_weight='balanced', penalty='l2', max_iter=150))
        model.fit(X_train, Y_train)

        return model, vectorizer, X_train, Y_train, x_test, y_test

    @staticmethod
    def test_evaluate_model(model, x_test, y_test):
        logging.info("Getting top-k predictions")
        preds, pred_model_proba = Worker.get_top_k_predictions(model,x_test,k=3)
        pred_model = model.predict(x_test)

        # GET PREDICTED VALUES AND GROUND TRUTH INTO A LIST OF LISTS - for ease of evaluation
        logging.info("Collecting predictions into a list...")
        eval_items = Worker.collect_preds(y_test,preds)
        
        # GET EVALUATION NUMBERS ON TEST SET -- HOW DID WE DO?
        logging.info("Computing evaluation metrics...")
        top_1_accuracy = top_k_accuracy_score(y_test, pred_model_proba, k=1)
        top_2_accuracy = top_k_accuracy_score(y_test, pred_model_proba, k=2)
        top_3_accuracy = top_k_accuracy_score(y_test, pred_model_proba, k=3)
        mrr_at_3 = Worker.compute_mrr_at_k(eval_items)
        top_1_precision = precision_score(y_test, pred_model, average='weighted', zero_division=1)
        top_1_recall = recall_score(y_test, pred_model, average='weighted', zero_division=1)
        top_1_f1score = f1_score(y_test, pred_model, average='weighted', zero_division=1)
        Worker.metrics['top_1_acc'] = top_1_accuracy
        Worker.metrics['top_2_acc'] = top_2_accuracy
        Worker.metrics['top_3_acc'] = top_3_accuracy
        Worker.metrics['mrr_at_3'] = mrr_at_3
        Worker.metrics['top_1_precision'] = top_1_precision
        Worker.metrics['top_1_recall'] = top_1_recall
        Worker.metrics['top_1_f1score'] = top_1_f1score

        logging.info('Classification Report')
        print(pd.DataFrame(classification_report(y_test, pred_model, output_dict=True, zero_division=1)).transpose())

        return top_3_accuracy, mrr_at_3, preds

    @staticmethod
    def get_metrics():
        return Worker.metrics

    @staticmethod
    def save_to_disk(model, vectorizer, cat):
        if(cat == 'parentCategoryId'):
            dump(model, os.getcwd() + '/model_data/model_macro_cat.joblib')
            with open(os.getcwd() + '/model_data/vectorizer_macro_cat.pk', 'wb') as fin:
                pickle.dump(vectorizer, fin)
            MODEL = 'model_macro_cat.joblib'
            VECTORIZER = 'vectorizer_macro_cat.pk'
            LABEL_ENCODING = 'parent_cat_encoding_close.json'
        elif(cat == 'categoryId'):
            dump(model, os.getcwd() + '/model_data/model_micro_cat.joblib')
            with open(os.getcwd() + '/model_data/vectorizer_micro_cat.pk', 'wb') as fin:
                pickle.dump(vectorizer, fin)
            MODEL = 'model_micro_cat.joblib'
            VECTORIZER = 'vectorizer_micro_cat.pk'
            LABEL_ENCODING = 'cat_encoding_close.json'

        minioClient = Minio(HOST_URI, access_key=ACCESS_KEY, secret_key=SECRET_KEY, secure=True if "amazonaws" in HOST_URI else False, region=REGION)
        
        try:
            object = minioClient.fput_object(
                bucket_name=BUCKET_NAME, object_name=MODEL, file_path=os.getcwd() + '/model_data/%s' % MODEL
            )
            logging.info("MODEL UPLOAD COMPLETED!")
            logging.info(object)
        except InvalidResponseError as err:
            logging.error(err)

        try:
            object = minioClient.fput_object(
                bucket_name=BUCKET_NAME, object_name=VECTORIZER, file_path=os.getcwd() + '/model_data/%s' % VECTORIZER
            )
            logging.info("VECTORIZER UPLOAD COMPLETED!")
            logging.info(object)
        except InvalidResponseError as err:
            logging.error(err)
        
        try:
            object = minioClient.fput_object(
                bucket_name=BUCKET_NAME, object_name=LABEL_ENCODING, file_path=os.getcwd() + '/model_data/%s' % LABEL_ENCODING
            )
            logging.info("LABELS UPLOAD COMPLETED!")
            logging.info(object)
        except InvalidResponseError as err:
            logging.error(err)
    
    @staticmethod
    def get_top_k_predictions(model,X_test,k):
        probs = model.predict_proba(X_test)
        best_n = np.argsort(probs, axis=1)[:,-k:]
        preds=[[model.classes_[predicted_cat] for predicted_cat in prediction] for prediction in best_n]
        preds=[item[::-1] for item in preds]     
        return preds, probs
    
    @staticmethod
    def collect_preds(Y_test,Y_preds):
        pred_gold_list=[[[Y_test[idx]],pred] for idx,pred in enumerate(Y_preds)]
        return pred_gold_list

    @staticmethod
    def _reciprocal_rank(true_labels: list, machine_preds: list):
        # add index to list only if machine predicted label exists in true labels
        tp_pos_list = [(idx + 1) for idx, r in enumerate(machine_preds) if r in true_labels]

        rr = 0
        if len(tp_pos_list) > 0:
            # for RR we need position of first correct item
            first_pos_list = tp_pos_list[0]
            
            # rr = 1/rank
            rr = 1 / float(first_pos_list)

        return rr

    @staticmethod
    def compute_mrr_at_k(items:list):
        """Compute the MRR (average RR) at cutoff k"""
        rr_total = 0
        
        for item in items:   
            rr_at_k = Worker._reciprocal_rank(item[0],item[1])
            rr_total = rr_total + rr_at_k
            mrr = rr_total / 1/float(len(items))

        return mrr
    
    @staticmethod
    def get_train_test_error(model, X_train, Y_train, x_test, y_test):
        Y_train_data_pred = model.predict(X_train)
        y_test_data_pred = model.predict(x_test)
        training_error = mean_absolute_error(Y_train, Y_train_data_pred) 
        testing_error = mean_absolute_error(y_test, y_test_data_pred)
        Worker.metrics['train_err'] = training_error
        Worker.metrics['test_err'] = testing_error    

    @staticmethod
    def _process(message):
        print(json.loads(message._record.value))
        return json.loads(message._record.value)
        '''logger.debug(f'Got new message:\n{message}\n')
        print(f'Got new message:\n{message}\n')'''
